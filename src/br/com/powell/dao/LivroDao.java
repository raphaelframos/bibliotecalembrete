package br.com.powell.dao;

import java.util.ArrayList;
import java.util.Calendar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import br.com.powell.model.Livro;
import br.com.powell.utils.ConstantsUtils;
import br.com.powell.utils.DateUtils;

public class LivroDao extends AbstractDao{

	public LivroDao(Context context) {
		super(context);
	}

	public void adiciona(Livro livro) {
		if(existe(livro.getId())){
			atualiza(livro);
		}else{
			insere(livro);
		}
		
	}

	private void atualiza(Livro livro) {
		ContentValues contentValues = montaContentValues(livro);
		getBancoEscrita().update(ConstantsUtils.TABELA_LIVRO, contentValues, ConstantsUtils.LIVRO_ID + " = ?", new String[] {livro.getId().toString()});
	}

	private boolean existe(Integer id) {
		try{
			String[] parametro = {id.toString()};
			Cursor cursorComLivro = getBancoLeitura().query(ConstantsUtils.TABELA_LIVRO, new String[] 
					{ConstantsUtils.LIVRO_ID}, ConstantsUtils.LIVRO_ID + " = ?", parametro, null, null, null);
			boolean existeLivro = cursorComLivro.getCount() > 0;
			cursorComLivro.close();
			return existeLivro;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

	private void insere(Livro livro) {
		ContentValues contentValues = montaContentValues(livro);
		getBancoEscrita().insert(ConstantsUtils.TABELA_LIVRO, null, contentValues);
		
	}

	private ContentValues montaContentValues(Livro livro) {
		ContentValues contentValues = new ContentValues();
		contentValues.put(ConstantsUtils.LIVRO_ID, livro.getId());
		contentValues.put(ConstantsUtils.LIVRO_NOME, livro.getNomeDoLivro());
		contentValues.put(ConstantsUtils.LIVRO_VENCIMENTO, livro.getVencimento());
		contentValues.put(ConstantsUtils.LIVRO_DEVOLVIDO, livro.isDevolvido());
		return contentValues;
	}

	public ArrayList<Livro> retornaLivrosEmprestados() {
		
		Cursor cursor = getBancoLeitura().query(ConstantsUtils.TABELA_LIVRO, null, null, null, null, null, ConstantsUtils.LIVRO_DEVOLVIDO + ", " + ConstantsUtils.LIVRO_VENCIMENTO);
		ArrayList<Livro> livros = new ArrayList<Livro>();
		while(cursor.moveToNext()){
			Livro livro = new Livro();
			livro.setNomeDoLivro(cursor.getString(cursor.getColumnIndex(ConstantsUtils.LIVRO_NOME)));
			livro.setVencimento(cursor.getLong(cursor.getColumnIndex(ConstantsUtils.LIVRO_VENCIMENTO)));
			livro.setId(cursor.getInt(cursor.getColumnIndex(ConstantsUtils.LIVRO_ID)));
			livro.setDevolvido(cursor.getInt(cursor.getColumnIndex(ConstantsUtils.LIVRO_DEVOLVIDO)));
			livros.add(livro);
		}
		cursor.close();
		return livros;
	}

	public void remove(Integer id) {
		getBancoEscrita().delete(ConstantsUtils.TABELA_LIVRO, ConstantsUtils.LIVRO_ID + " = ?", new String[] {id.toString()});
	}

	public boolean existeLivroVencido() {
		try{
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(DateUtils.adicionaDiaParaData(DateUtils.retornaCalendarComDataAtual(), -1));
			String[] parametro = {DateUtils.retornaDataAtualLong().toString(), "0"};
			Cursor cursorComLivroVencido = getBancoLeitura().query(ConstantsUtils.TABELA_LIVRO, 
					new String[] {ConstantsUtils.LIVRO_ID}, ConstantsUtils.LIVRO_VENCIMENTO + " < ? AND " + ConstantsUtils.LIVRO_DEVOLVIDO + " =?", parametro, null, null, null);
			boolean existeLivro = cursorComLivroVencido.getCount() > 0;
			cursorComLivroVencido.close();
			return existeLivro;
		}catch(Exception e){
			return false;
		}
		
	}

	public boolean existeLivroVencendoHoje() {
		try{
			String[] parametro = {DateUtils.retornaDataAtualLong().toString(), "0"};
			Cursor cursorComLivroVencido = getBancoLeitura().query(ConstantsUtils.TABELA_LIVRO, 
					new String[] {ConstantsUtils.LIVRO_ID}, ConstantsUtils.LIVRO_VENCIMENTO + " = ? AND " + ConstantsUtils.LIVRO_DEVOLVIDO + " =?", parametro, null, null, null);
			boolean existeLivro = cursorComLivroVencido.getCount() > 0;
			cursorComLivroVencido.close();
			return existeLivro;
		}catch(Exception e){
			return false;
		}
	}

}
