package br.com.powell.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import br.com.powell.db.DBHelper;

public abstract class AbstractDao{
	
	private DBHelper dbHelper;

	public AbstractDao(Context context) {
		dbHelper = new DBHelper(context);
	}
	
	public SQLiteDatabase getBancoEscrita(){
		return dbHelper.getWritableDatabase();
	}
	
	public SQLiteDatabase getBancoLeitura(){
		return dbHelper.getReadableDatabase();
	}
	
	public void fechaBanco(){
		try{
			dbHelper.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
