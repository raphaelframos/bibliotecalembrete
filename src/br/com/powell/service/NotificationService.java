package br.com.powell.service;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import br.com.powell.biblioteca.Entrada;
import br.com.powell.biblioteca.R;
import br.com.powell.dao.LivroDao;
import br.com.powell.utils.NotificationUtil;

public class NotificationService extends Service{

	public static final String NOTIFICATION_INTENT = "br.com.powell.biblioteca.Push_Notification";
	private static final String FROM_NOTIFICATION_EXTRA_KEY = "br.com.powell.biblioteca.notification";

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public void onCreate() {
		String mensagem = "";
		criarNotificacao(mensagem);
		cancelaAlarmes();
		sendBroadcast(new Intent(this, NotificationReceiver.class));
		stopSelf();
	}

	
	private void cancelaAlarmes() {
		PendingIntent pendingIntent = PendingIntent.getService(
				NotificationService.this, 0,
				new Intent(NOTIFICATION_INTENT), PendingIntent.FLAG_NO_CREATE);
		if (pendingIntent != null) {
			pendingIntent.cancel();
		}
	}

	private void criarNotificacao(String mensagem) {
		Intent it = new Intent(this, Entrada.class);
		it.putExtra(FROM_NOTIFICATION_EXTRA_KEY, true);
		LivroDao livroDao = new LivroDao(getApplicationContext());
		if(livroDao.existeLivroVencido()){
			NotificationUtil.create(getApplicationContext(), getString(R.string.vencido), getString(R.string.atencao), getString(R.string.emprestimo_vencido), R.drawable.ic_launcher, R.drawable.ic_launcher, it);
		}
		
		if(livroDao.existeLivroVencendoHoje()){
			NotificationUtil.create(getApplicationContext(), getString(R.string.vencendo), getString(R.string.atencao), getString(R.string.emprestimo_vencendo_hoje), R.drawable.ic_launcher, R.drawable.ic_launcher, it);
		}
	}

}