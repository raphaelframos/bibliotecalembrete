package br.com.powell.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import br.com.powell.biblioteca.R;
import br.com.powell.model.Livro;

public class AdapterLivro extends ArrayAdapter<Livro>{
	
	private ArrayList<Livro> livros;

	public AdapterLivro(Context context, ArrayList<Livro> livros) {
		super(context, R.layout.activity_emprestimos, livros);
		this.livros = livros;
	}
	
	@Override
	public View getView(int posicao, View view, ViewGroup parent){
		LivroViewHolder livroVH = null;
		if(view == null){
			livroVH = new LivroViewHolder();
			LayoutInflater inflate = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflate.inflate(R.layout.adapter_livro, parent, false);
			livroVH.textNomeDoLivro = (TextView) view.findViewById(R.id.textViewNomeDoLivro);
			livroVH.textDiasRestantes = (TextView) view.findViewById(R.id.textViewDiasRestantes);
			livroVH.textTextoDias = (TextView) view.findViewById(R.id.textViewTextoDias);
			view.setTag(livroVH);
		}
		livroVH = (LivroViewHolder) view.getTag();
		Livro livro = livros.get(posicao);
		if(livro != null){
			livroVH.textNomeDoLivro.setText(livro.getNomeDoLivro());
			if(livro.isDevolvido()){
				livroVH.textDiasRestantes.setVisibility(View.GONE);
				livroVH.textTextoDias.setText(getContext().getResources().getString(R.string.devolvido));
			}else{
				livroVH.textDiasRestantes.setVisibility(View.VISIBLE);
				int diasRestantes = livro.getDiasRestantes();
				livroVH.textDiasRestantes.setText(String.valueOf(diasRestantes));
				livroVH.textDiasRestantes.setTextColor(defineCorDoTextoDosDiasRestantes(diasRestantes));
				livroVH.textTextoDias.setText(defineTexto(diasRestantes));
			}
		}
		
		return view;
		
	}
	
	private String defineTexto(int diasRestantes) {
		if(diasRestantes == -1 || diasRestantes == 1){
			return getContext().getResources().getString(R.string.dia);
		}else{
			return getContext().getResources().getString(R.string.dias);
		}
	}

	private int defineCorDoTextoDosDiasRestantes(int diasRestantes) {
		if(diasRestantes < 0){
			return getContext().getResources().getColor(R.color.vermelho);
		}else{
			return getContext().getResources().getColor(R.color.verde);
		}	
	}

	public static class LivroViewHolder{
		TextView textNomeDoLivro;
		TextView textDiasRestantes;
		TextView textTextoDias;
	}

	public void atualiza(ArrayList<Livro> livros) {
		clear();
		for(Livro livro : livros){
			this.add(livro);
		}
		this.notifyDataSetChanged();
		this.notifyDataSetInvalidated();
	}

}
