package br.com.powell.utils;

public class ConstantsUtils {

	public static final String NOME_BANCO = "biblioteca.db";
	public static final int VERSAO_BANCO = 1;
	public static final String TABELA_LIVRO = "LIVRO";
	public static final String LIVRO_ID = "ID";
	public static final String LIVRO_NOME = "NOME";
	public static final String LIVRO_VENCIMENTO = "VENCIMENTO";
	public static final String LIVRO_DEVOLVIDO = "DEVOLVIDO";
	public static final String EDICAO = "EDICAO";
	public static final String BROADCAST_NAME = "br.com.powell.controller.android.action.broadcast";
}
