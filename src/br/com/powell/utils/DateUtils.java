package br.com.powell.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtils {

	private static final String DATA_GATEWAY = "dd/MM/yyyy";
	private static final String MSGDATA = "Data Invalida!";

	public static String dateToString(Date data) {
		return formataDatetimeToString(data, DATA_GATEWAY);
	}

	public static String dateToString(Date data, String formatoData) {
		return formataDatetimeToString(data, formatoData);
	}
	
	private static String formataDatetimeToString(Date data, String param) {

		String ret = null;
		if (data == null) {
			return "";
		}
		
		try{
			SimpleDateFormat format = new SimpleDateFormat(param);
			ret = format.format(data);
		}catch(Exception e){
			SimpleDateFormat format = new SimpleDateFormat(DATA_GATEWAY);
			ret = format.format(data);
		}
		
		return ret;

	}

	public static Date stringToDate(String data)
			throws IllegalArgumentException, ParseException {
		if (data == null)
			return null;
		return formataStringToDatetime(data, DATA_GATEWAY, MSGDATA);
	}

	public static Date stringToDate(String data, String formato)
			throws IllegalArgumentException, ParseException {
		if (data == null)
			return null;
		return formataStringToDatetime(data, formato, MSGDATA);
	}

	private static Date formataStringToDatetime(String data, String param,
			String msg) throws IllegalArgumentException, ParseException {

		Date _data = null;
		if (data == null || data.length() == 0) {
			return null;
		}

		SimpleDateFormat format = new SimpleDateFormat(param);

		try {
			format.setLenient(false);
			_data = format.parse(data);
		} catch (Exception pe) {
			_data = format.parse("01/01/1900");
			throw new IllegalArgumentException("Data inv�lida");
		}

		return _data;
	}

	public static Date lowDateTime(Date date) {
		Calendar aux = Calendar.getInstance();
		aux.setTime(date);
		toOnlyDate(aux); // zera os parametros de hour,min,sec,milisec
		return aux.getTime();
	}

	public static Date highDateTime(Date date) {
		Calendar aux = Calendar.getInstance();
		aux.setTime(date);
		toOnlyDate(aux); // zera os parametros de hour,min,sec,milisec
		aux.add(Calendar.DATE, 1); // vai para o dia seguinte
		aux.roll(Calendar.MILLISECOND, false); // reduz 1 milisegundo
		return aux.getTime();
	}

	public static void toOnlyDate(Calendar date) {
		zeraTempoCalendar(date);
		date.set(Calendar.MILLISECOND, 0);
	}

	public static boolean verificaSeDataNaoPassou(Date dataEvento) {
		Calendar calendar = new GregorianCalendar();
		toOnlyDate(calendar);
		Date dataAtual = calendar.getTime();
		return dataEvento.compareTo(dataAtual) >= 0;
	}

	public static Long stringDemonstracaoToLong(String data)
			throws IllegalArgumentException, ParseException {
		Date date = stringToDate(data);
		return date.getTime();
	}

	public static Calendar retornaCalendarComDataAtual() {
		Calendar calendar = Calendar.getInstance();
		return zeraTempoCalendar(calendar);
	}

	public static Calendar zeraTempoCalendar(Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar;
	}

	public static Long retornaDataAtualLong() {
		return retornaCalendarComDataAtual().getTimeInMillis();
	}

	public static CharSequence formataDataCompleta(Calendar data) {
		DateFormat df = DateFormat.getDateInstance(DateFormat.FULL);
		return df.format(data.getTime());
	}

	public static Integer diferencaEmDiasEntreAsDatas(Long dataReferencia,
			Long dataExtra) {
		int dias = 0;
		try {     
			long differenceMilliSeconds = dataReferencia - dataExtra; 
			dias = (int) (differenceMilliSeconds/1000/60/60/24);           
		   } catch (Exception e) {     
		      throw new IllegalArgumentException(e.getMessage());
		   }     
		return dias;
	}
	
	public static Date adicionaDiaParaData(Calendar calendar, int dias) {
		Date _data = null;
		if (calendar != null) {
			calendar.add(Calendar.DATE, dias);
			_data = calendar.getTime();
		}
		return _data;
	}
}
