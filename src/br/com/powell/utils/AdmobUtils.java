package br.com.powell.utils;

import android.app.Activity;
import android.view.View;
import android.widget.LinearLayout;
import br.com.powell.biblioteca.R;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class AdmobUtils {
	
	public static void cria(Activity activity) {
		LinearLayout linearFundo = (LinearLayout) activity.findViewById(R.id.layoutAdMobs);
		linearFundo.setVisibility(View.VISIBLE);
		AdView adView = (AdView) activity.findViewById(R.id.adView);
	    AdRequest adRequest = new AdRequest.Builder()
	        .build();
	    adView.loadAd(adRequest);
	}

	public static void cria(Activity activity, View view) {
		AdView adView = (AdView) view.findViewById(R.id.adView);
	    AdRequest adRequest = new AdRequest.Builder()
	        .build();
	    adView.loadAd(adRequest);
	}
}
