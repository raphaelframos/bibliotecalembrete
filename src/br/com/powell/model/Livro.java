package br.com.powell.model;

import java.io.Serializable;

import br.com.powell.utils.DateUtils;

public class Livro implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String nomeDoLivro;
	private Long vencimento;
	private boolean devolvido;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNomeDoLivro() {
		return nomeDoLivro;
	}
	public void setNomeDoLivro(String nomeDoLivro) {
		this.nomeDoLivro = nomeDoLivro;
	}
	public Long getVencimento() {
		return vencimento;
	}
	public void setVencimento(Long vencimento) {
		this.vencimento = vencimento;
	}
	public Integer getDiasRestantes() {
		Long dataHoje = DateUtils.retornaDataAtualLong(); 
		return DateUtils.diferencaEmDiasEntreAsDatas(vencimento, dataHoje);
	}
	public boolean isDevolvido() {
		return devolvido;
	}
	public void setDevolvido(boolean devolvido) {
		this.devolvido = devolvido;
	}
	public void setDevolvido(int devolvido) {
		if(devolvido == 1){
			setDevolvido(true);
		}else{
			setDevolvido(false);
		}
	}

}
