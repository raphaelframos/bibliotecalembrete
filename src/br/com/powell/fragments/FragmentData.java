package br.com.powell.fragments;

import java.util.Calendar;

import br.com.powell.biblioteca.NovoEmprestimo;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class FragmentData extends DialogFragment{
	
	private OnDateSetListener onDate;
	private Calendar dataEscolhida;
	
	@Override
	public void setArguments(Bundle bundle){
		super.setArguments(bundle);
		dataEscolhida = (Calendar) bundle.getSerializable(NovoEmprestimo.DATA_ESCOLHIDA);
	}
	
	public void setOnDateSetListener(OnDateSetListener onDate){
		this.onDate = onDate;
	}
	
	@Override
	public Dialog onCreateDialog(Bundle bundle){
		return new DatePickerDialog(getActivity(), onDate,  dataEscolhida.get(Calendar.YEAR),  dataEscolhida.get(Calendar.MONTH),  dataEscolhida.get(Calendar.DAY_OF_MONTH));
	}

}
