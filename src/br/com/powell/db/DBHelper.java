package br.com.powell.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import br.com.powell.utils.ConstantsUtils;

public class DBHelper extends SQLiteOpenHelper {

	public static final String CRIA_LIVRO = "CREATE TABLE "
			+ ConstantsUtils.TABELA_LIVRO + "(" + ConstantsUtils.LIVRO_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ ConstantsUtils.LIVRO_NOME + " TEXT, "
			+ ConstantsUtils.LIVRO_VENCIMENTO + " LONG, " + ConstantsUtils.LIVRO_DEVOLVIDO + " BOOLEAN)";

	public DBHelper(Context context) {
		super(context, ConstantsUtils.NOME_BANCO, null,
				ConstantsUtils.VERSAO_BANCO);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CRIA_LIVRO);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

}
