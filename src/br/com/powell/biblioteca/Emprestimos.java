package br.com.powell.biblioteca;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.PopupMenu.OnMenuItemClickListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import br.com.powell.adapter.AdapterLivro;
import br.com.powell.dao.LivroDao;
import br.com.powell.model.Livro;
import br.com.powell.utils.AdmobUtils;
import br.com.powell.utils.ConstantsUtils;

public class Emprestimos extends ActionBarActivity {

	private LivroDao livroDao;
	private ArrayList<Livro> livros = new ArrayList<Livro>();
	private ListView listaDeLivros;
	private AdapterLivro adapterLivro;
	private static final int DEVOLVIDO = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_emprestimos);
		listaDeLivros = (ListView) findViewById(R.id.listViewLivros);
		iniciaDadosDaActivity();
		listaDeLivros.setAdapter(adapterLivro);
		listaDeLivros.setOnItemClickListener(selecionaOpcaoDoItemDaLista());
	}

	private OnItemClickListener selecionaOpcaoDoItemDaLista() {
		return new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View view,
					int posicao, long id) {
				final Livro livro = (Livro) adapter.getItemAtPosition(posicao);
				PopupMenu popupMenu = new PopupMenu(Emprestimos.this, view);
				if(!livro.isDevolvido()){
					popupMenu.getMenu().add(0, DEVOLVIDO, 0,
							getResources().getString(R.string.devolvido));
				}
				popupMenu.getMenuInflater().inflate(R.menu.livro_emprestimo,
						popupMenu.getMenu());
				popupMenu
						.setOnMenuItemClickListener(new OnMenuItemClickListener() {

							@Override
							public boolean onMenuItemClick(MenuItem menuItem) {

								switch (menuItem.getItemId()) {
								
								case DEVOLVIDO:
									livro.setDevolvido(true);
									livroDao.adiciona(livro);
									atualizaTela();
									break;
									
								case R.id.action_editar:
									Intent it = new Intent(Emprestimos.this, NovoEmprestimo.class);
									it.putExtra(ConstantsUtils.EDICAO, livro);
									startActivity(it);
									break;

								case R.id.action_remover:
									livroDao.remove(livro.getId());
									atualizaTela();
									break;

								default:
									break;
								}
								return false;
							}
						});
				popupMenu.show();
			}
		};
	}

	private void iniciaDadosDaActivity() {
		livroDao = new LivroDao(getApplicationContext());
		adapterLivro = new AdapterLivro(getApplicationContext(), livros);
		AdmobUtils.cria(this);
		getSupportActionBar().setTitle("");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.emprestimos, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case R.id.action_novo_emprestimo:
			Intent it = new Intent(Emprestimos.this, NovoEmprestimo.class);
			startActivity(it);
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onResume() {
		super.onResume();
		atualizaTela();
	}

	private void atualizaTela() {
		livros = livroDao.retornaLivrosEmprestados();
		adapterLivro.atualiza(livros);
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		try{
			livroDao.fechaBanco();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
