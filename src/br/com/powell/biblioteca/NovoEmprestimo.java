package br.com.powell.biblioteca;

import java.util.Calendar;

import android.app.DatePickerDialog.OnDateSetListener;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;
import br.com.powell.dao.LivroDao;
import br.com.powell.fragments.FragmentData;
import br.com.powell.model.Livro;
import br.com.powell.utils.ConstantsUtils;
import br.com.powell.utils.DateUtils;

public class NovoEmprestimo extends ActionBarActivity {

	public static final String DATA_ESCOLHIDA = "DATAESCOLHIDA";
	private EditText editNomeDoLivro;
	private Button buttonVencimento;
	private Calendar vencimento;
	private LivroDao livroDao;
	private Livro livro;

	@Override
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.novo_emprestimo);
		editNomeDoLivro = (EditText) findViewById(R.id.editTextNomeLivro);
		buttonVencimento = (Button) findViewById(R.id.buttonVencimento);

		vencimento = DateUtils.retornaCalendarComDataAtual();
		livroDao = new LivroDao(this);
		livro = (Livro) getIntent().getSerializableExtra(ConstantsUtils.EDICAO);
		if(estaEditando()){
			getSupportActionBar().setTitle(getString(R.string.editando_emprestimo));
			editNomeDoLivro.setText(livro.getNomeDoLivro());
			vencimento.setTimeInMillis(livro.getVencimento());
		}
		atualizaVencimento();
		buttonVencimento.setOnClickListener(defineVencimento);
	}

	private void salvaEmprestimo() {
		try {
			if(camposEstaoPreenchidos()){
				if(!estaEditando()){
					livro = new Livro();
				}
				String nomeDoLivro = editNomeDoLivro.getText().toString();
				livro.setNomeDoLivro(nomeDoLivro);
				livro.setVencimento(vencimento.getTimeInMillis());
				livroDao.adiciona(livro);
				Toast.makeText(getApplicationContext(), getString(R.string.livro_adicionado), Toast.LENGTH_SHORT).show();
				finish();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private boolean camposEstaoPreenchidos() {
		String nomeDoLivro = editNomeDoLivro.getText().toString();
		if(nomeDoLivro.equals("")){
			editNomeDoLivro.setError(getString(R.string.campo_branco));
			Toast.makeText(getApplicationContext(), getString(R.string.campo_branco), Toast.LENGTH_SHORT).show();
			return false;
		}
		
		return true;
	}

	private boolean estaEditando() {
		return livro != null;
	}

	private OnDateSetListener onDate = new OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			vencimento.set(Calendar.DAY_OF_MONTH, dayOfMonth);
			vencimento.set(Calendar.MONTH, monthOfYear);
			vencimento.set(Calendar.YEAR, year);
			atualizaVencimento();
		}
	};
	
	private void atualizaVencimento() {
		buttonVencimento.setText(DateUtils.formataDataCompleta(vencimento));
	}

	private OnClickListener defineVencimento = new OnClickListener() {

		@Override
		public void onClick(View v) {
			FragmentData fragmentData = new FragmentData();
			Bundle bundle = new Bundle();
			bundle.putSerializable(DATA_ESCOLHIDA, vencimento);
			fragmentData.setArguments(bundle);
			fragmentData.setOnDateSetListener(onDate);
			fragmentData.show(getSupportFragmentManager(),
					getString(R.string.novo_emprestimo));
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.novo_emprestimo, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
		switch (menuItem.getItemId()) {
		case R.id.action_salvar:
			salvaEmprestimo();
			break;

		default:
			break;
		}
		return true;

	}

}
