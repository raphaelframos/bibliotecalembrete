package br.com.powell.biblioteca;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import br.com.powell.service.NotificationReceiver;
import br.com.powell.utils.ConstantsUtils;

public class Entrada extends Activity {

    private static final long TEMPO_ENTRADA = 500;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrada);
        iniciaServicoDeAlarme();
        new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				startActivity(new Intent(Entrada.this, Emprestimos.class));
				finish();
			}
		}, TEMPO_ENTRADA);
    }

	private void iniciaServicoDeAlarme() {
		IntentFilter intentFilter = new IntentFilter(ConstantsUtils.BROADCAST_NAME);
		getApplicationContext().registerReceiver(new NotificationReceiver(), intentFilter);
		Intent intent = new Intent(ConstantsUtils.BROADCAST_NAME);  
		sendBroadcast(intent);
	}
}
